# Project 2
Create a LWC/Aura Experience Cloud site & imperative business solutions.

This will be a team-based project. As a team, developers should decide on a business that they want to model in Salesforce (this should not be an extension of Project 0/Project 1, given the knowledge and comfortability they’ve developed with the Salesforce platform since the time that they started those projects).

The team should leverage Apex and Experience Cloud to provide a client-facing experience site that integrates with the Salesforce org. At a minimum, each developer on the team must complete

- A Lightning web component containing some feature(s) relevant to the business that’s exposed on an Experience site
- An Aura component that interacts with the LWC developed by a different member of the team
- An Apex class to be used in conjunction with the components
- Any additional declarative customizations required to support the feature(s)
- An Apex Trigger for the business that follows best practices
- Comprehensive Apex test classes for the Apex used in the trigger and Apex controllers
- Package Based Development
- The team will be required to develop using the Package Development Model. At minimum, each developer will be required to create at least one unlocked package. It is recommended that the team create a base package containing metadata (e.g. objects and fields) that many of the customizations will build off of, so that other packages can list this as a dependency.

## Stretch Goals
### Data Visualization

- Actionable reports and dashboards relating to the business the developer has chosen to model.
- Deployment - Successfully deploy the experience site in addition to other customizations during the presentation.

- Presentation and Styling - The team must ensure that the experience cloud site contains uniform customized styling to denote it as a polished and branded experience. Styling will require the use of CSS and leveraging SLDS with consistent styling for HTML elements. Additionally, the team will be required to have the entire project on a GitHub repo and follow reasonable branching procedures. Finally, as part of the live demo, the team will clone the master branch of the repo and push all custom metadata (with the possible exception of the Experience site) to a fresh org.

*** Notes ***
Each member of the team should have a speaking role in the presentation. As a result, the presentation will require teamwork and coordination to come across as a unified and polished endeavor and take no longer than a combined 20 minutes.